#include "catch.hpp"
#include "topology.hpp"
#include <vector>

TEST_CASE("Grid uses subscript operator") {
	sudoku::Grid<> g;
	g[{1,2}];
}

TEST_CASE("Grid actually stores cells under coordinates") {
	sudoku::Grid<3, int> g;
	g[{1,2}] = 42;
	int v = g[{1,2}];
	REQUIRE(v == 42);
}

TEST_CASE("Grid subscript operator throws on invalid coord") {
	sudoku::Grid<3, int> g;
	sudoku::Coord zero{0,0};
	CHECK_THROWS_AS(g[zero], sudoku::InvalidCoord);
	sudoku::Coord one{1,1};
	CHECK_NOTHROW(g[one]);
}

TEST_CASE("Grid correctly retains values while copying") {
	sudoku::Grid<3, int> source;
	for (int r = 1; r <= 9; r++) {
		for (int c = 1; c <= 9; c++) {
			source[{r,c}] = 10*r + c;
		}
	}
	sudoku::Grid<3, int> dest{source};
	sudoku::Grid<3, int> dest2{};
	dest2 = source;
	for (int r = 1; r <= 9; r++) {
		for (int c = 1; c <= 9; c++) {
			auto v = dest[{r,c}];
			auto v2 = dest2[{r,c}];
			REQUIRE(v == 10*r + c);
			REQUIRE(v2 == 10*r + c);
		}
	}
}

TEST_CASE("Grid is navigable by rows, cols, boxes") {
	sudoku::Grid<3, int> g;
	for (int r = 1; r <= 9; r++) {
		for (int c = 1; c <= 9; c++) {
			g[{r,c}] = 10*r + c;
		}
	}

	SECTION("Rows are enumerated in ascending column order") {
		std::vector<std::pair<sudoku::Coord, int>> row {
			{{5,1}, 51}, {{5,2}, 52}, {{5,3}, 53},
			{{5,4}, 54}, {{5,5}, 55}, {{5,6}, 56},
			{{5,7}, 57}, {{5,8}, 58}, {{5,9}, 59}
		};
		auto it = row.begin();
		for (auto&& cell : g.row(5)) {
			REQUIRE(cell == it->second);
			cell = 0;
			REQUIRE(g[it->first] == 0);
			++it;
		}
		CHECK(it == row.end());
	}
	SECTION("Columns are enumerated in ascending row order") {
		std::vector<std::pair<sudoku::Coord, int>> col {
			{{1,5}, 15}, {{2,5}, 25}, {{3,5}, 35},
			{{4,5}, 45}, {{5,5}, 55}, {{6,5}, 65},
			{{7,5}, 75}, {{8,5}, 85}, {{9,5}, 95}
		};
		auto it = col.begin();
		for (auto&& cell : g.col(5)) {
			REQUIRE(cell == it->second);
			cell = 0;
			REQUIRE(g[it->first] == 0);
			++it;
		}
		CHECK(it == col.end());
	}
	SECTION("Boxes are enumerated left-to-right top-to-bottom") {
		std::vector<std::pair<sudoku::Coord, int>> box {
			{{4,4}, 44}, {{4,5}, 45}, {{4,6}, 46},
			{{5,4}, 54}, {{5,5}, 55}, {{5,6}, 56},
			{{6,4}, 64}, {{6,5}, 65}, {{6,6}, 66}
		};
		auto it = box.begin();
		for (auto&& cell : g.box(5)) {
			REQUIRE(cell == it->second);
			cell = 0;
			REQUIRE(g[it->first] == 0);
			++it;
		}
		CHECK(it == box.end());
	}
}


