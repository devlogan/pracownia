#include "catch.hpp"
#include "topology.hpp"

TEST_CASE("Canonical sudoku order is 3") {
	REQUIRE(sudoku::CANONICAL_ORDER == 3);
}

TEST_CASE("Board coordinates are stored as int pairs") {
	sudoku::Coord p{1,2};
	INFO("The order of members should be (row,col)");
	CHECK(p.row() == 1);
	CHECK(p.col() == 2);
}

TEST_CASE("Board coordinates are copyable and equatable") {
	sudoku::Coord p{1,2};
	sudoku::Coord q{p};
	CHECK(q == p);
	sudoku::Coord r{2,1};
	CHECK(r != p);
}

TEST_CASE("Board coordinates are comparable") {
	sudoku::Coord tl{1,1};
	sudoku::Coord tr{1,2};
	sudoku::Coord bl{2,1};
	sudoku::Coord br{2,2};
	CHECK(tl < tr); CHECK(tl < bl);
	CHECK(br > bl); CHECK(br > tr);
	CHECK(tl <= tl); CHECK(tl <= tr);
	CHECK(br >= br); CHECK(br >= bl);
}

TEST_CASE("Grid is a template with default order 3") {
	sudoku::Grid<> grid;
	CHECK(grid.order == 3);
	CHECK(grid.size == 9);
	sudoku::Grid<4> grid4;
	CHECK(grid4.order == 4);
	CHECK(grid4.size == 16);
	CHECK(sudoku::Grid<5>::order == 5);
	CHECK(sudoku::Grid<5>::size == 25);
}

TEST_CASE("Grid can tell if a given coord is valid") {
	CHECK(sudoku::Grid<>::isValidCoord({1,1}));
	CHECK(sudoku::Grid<>::isValidCoord({9,9}));
	CHECK_FALSE(sudoku::Grid<>::isValidCoord({0,0}));
	CHECK_FALSE(sudoku::Grid<>::isValidCoord({10,10}));
	CHECK_FALSE(sudoku::Grid<>::isValidCoord({10,5}));
	CHECK_FALSE(sudoku::Grid<>::isValidCoord({5,10}));
}

TEST_CASE("Grid can calculate row, col and box no. of a coord") {
	CHECK(sudoku::Grid<>::rowOf({2,3}) == 2);
	CHECK(sudoku::Grid<>::colOf({2,3}) == 3);
	CHECK(sudoku::Grid<>::boxOf({2,3}) == 1);
	CHECK(sudoku::Grid<>::boxOf({3,7}) == 3);
	CHECK(sudoku::Grid<>::boxOf({4,6}) == 5);
	CHECK(sudoku::Grid<>::boxOf({7,2}) == 7);
	CHECK(sudoku::Grid<>::rowOf({10,10}) == 0);
	CHECK(sudoku::Grid<>::colOf({10,10}) == 0);
	CHECK(sudoku::Grid<>::boxOf({10,10}) == 0);
}

TEST_CASE("Grid can tell if a given box number is valid") {
	CHECK(sudoku::Grid<>::isValidBox(1));
	CHECK(sudoku::Grid<>::isValidBox(9));
	CHECK_FALSE(sudoku::Grid<>::isValidBox(0));
	CHECK_FALSE(sudoku::Grid<>::isValidBox(10));
}

TEST_CASE("Grid can calculate top-left origin of a box") {
	CHECK(sudoku::Grid<>::originOfBox(1) == sudoku::Coord(1,1));
	CHECK(sudoku::Grid<>::originOfBox(4) == sudoku::Coord(4,1));
	CHECK(sudoku::Grid<>::originOfBox(6) == sudoku::Coord(4,7));
	CHECK(sudoku::Grid<>::originOfBox(8) == sudoku::Coord(7,4));
}

