#pragma once
#include <utility>
#include "defaults.hpp"
#include "cell.hpp"
#include <exception>
#include <memory>

namespace sudoku {

	struct Coord : std::pair<int, int> {
		using std::pair<int, int>::pair;
		int& row() {
			return first;
		}
		int row() const {
			return first;
		}
		int& col() {
			return second;
		}
		int col() const {
			return second;
		}
	};

	class InvalidCoord : virtual public std::exception {};

	template <int ORDER = CANONICAL_ORDER,
			  typename C = Cell<ORDER>>
	class Grid {
	public:
		static constexpr int order{ORDER};
		static constexpr int size{ORDER*ORDER};

	protected:
		class SubViewImpl {
		public:
			virtual Coord begin() = 0;
			virtual Coord end() = 0;
			virtual Coord next(Coord) = 0;
		};
		class RowViewImpl : public SubViewImpl {
			int row_;
		public:
			RowViewImpl(int row) : row_{row} {}
			virtual Coord begin() {
				return {row_, 1};
			}
			virtual Coord end() {
				return {row_, size + 1};
			}
			virtual Coord next(Coord coord) {
				++coord.col();
				return coord;
			}
		};
		class ColViewImpl : public SubViewImpl {
			int col_;
		public:
			ColViewImpl(int col) : col_{col} {}
			virtual Coord begin() {
				return {1, col_};
			}
			virtual Coord end() {
				return {size + 1, col_};
			}
			virtual Coord next(Coord coord) {
				++coord.row();
				return coord;
			}
		};
		class BoxViewImpl : public SubViewImpl {
			Coord origin_;
		public:
			BoxViewImpl(Coord origin) : origin_{origin} {}
			virtual Coord begin() {
				return origin_;
			}
			virtual Coord end() {
				return {origin_.row() + order, origin_.col()};
			}
			virtual Coord next(Coord coord) {
				++coord.col();
				if (coord.col() >= origin_.col() + order) {
					coord.col() = origin_.col();
					++coord.row();
				}
				return coord;
			}
		};

	public:
		class SubView {
			Grid& grid_;
			std::shared_ptr<SubViewImpl> implementation_;
		public:
			class Iterator {
				Grid& grid_;
				std::shared_ptr<SubViewImpl> implementation_;
				Coord coord_;
			public:
				static constexpr struct End{} end{};

				Iterator(Grid& grid,
						 std::shared_ptr<SubViewImpl> impl)
					:	grid_(grid),
						implementation_{std::move(impl)},
						coord_{implementation_->begin()}
					{}
				Iterator(Grid& grid,
						 std::shared_ptr<SubViewImpl> impl,
						 End)
					:	grid_(grid),
						implementation_{std::move(impl)},
						coord_{implementation_->end()}
					{}

				bool operator!=(const Iterator& other) {
					return &grid_ != &other.grid_ ||
						coord_ != other.coord_;
				}
				C& operator*() {
					return grid_[coord_];
				}
				Iterator& operator++() {
					coord_ = implementation_->next(coord_);
					return *this;
				}
			};
		public:
			Iterator begin() const {
				return {grid_, implementation_};
			}
			Iterator end() const {
				return {grid_, implementation_, Iterator::end};
			}
		protected:
			SubView(Grid& grid, std::shared_ptr<SubViewImpl>&& impl)
				:	grid_(grid), implementation_{impl}
				{}
			friend Grid;
		};

	private:
		C cells_[size][size];

	public:
		C& operator[](const Coord& coord) {
			if (!isValidCoord(coord)) {
				throw InvalidCoord{};
			}
			return cells_[coord.row()-1][coord.col()-1];
		}
		SubView row(int r) {
			return {*this, std::make_shared<RowViewImpl>(r)};
		}
		SubView col(int c) {
			return {*this, std::make_shared<ColViewImpl>(c)};
		}
		SubView box(int b) {
			return {*this, std::make_shared<BoxViewImpl>(originOfBox(b))};
		}

	public:
		static bool isValidCoord(Coord coord) {
			return coord.row() >= 1 && coord.row() <= size
				&& coord.col() >= 1 && coord.col() <= size;
		}
		static bool isValidBox(int box) {
			return box >= 1 && box <= size;
		}
		static int rowOf(Coord coord) {
			return isValidCoord(coord) ? coord.row() : 0;
		}
		static int colOf(Coord coord) {
			return isValidCoord(coord) ? coord.col() : 0;
		}
		static int boxOf(Coord coord) {
			if (!isValidCoord(coord)) {
				return 0;
			}
			int boxBase = ((coord.row()-1) / order) * order;
			int boxOffset = (coord.col()-1) / order + 1;
			return boxBase + boxOffset;
		}
		static Coord originOfBox(int box) {
			int topRow = ((box-1) / order) * order + 1;
			int leftCol = ((box-1) % order) * order + 1;
			return {topRow, leftCol};
		}
	};

	template <int ORDER, typename C>
	constexpr int Grid<ORDER, C>::order;

	template <int ORDER, typename C>
	constexpr int Grid<ORDER, C>::size;

}
